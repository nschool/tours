<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\TourListSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tour Lists';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">

<div class="tour-list-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Створити новий тур', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?  Pjax::begin();?>
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemOptions' => ['class' => 'item'],
        'itemView' => function ($model, $key, $index) {

            return  $this->render('_view', ['model' => $model, 'index'=>$index]);

        },
        'pager' => [
            'firstPageLabel' => '<i class="fa fa-angle-double-left"></i>',
            'lastPageLabel' => '<i class="fa fa-angle-double-right"></i>',
            'prevPageLabel' => '<i class="fa  fa-angle-left"></i>',
            'nextPageLabel' => '<i class="fa  fa-angle-right"></i>',
            'maxButtonCount' => 3,
            'options' => [
                'tag' => 'ul',
                'class' => 'pagination',
            ],
        ],
    ]) ?>

    <?  Pjax::end();?>
<!--    --><?//= GridView::widget([
//        'dataProvider' => $dataProvider,
//        'filterModel' => $searchModel,
//        'columns' => [
//            ['class' => 'yii\grid\SerialColumn'],
//
//            'tour_list_id',
//            'name',
//            'description:ntext',
//            'price',
//            'country',
//             'city',
//             'date_entrance',
//             'date_departure',
//
//            ['class' => 'yii\grid\ActionColumn'],
//        ],
//    ]); ?>
</div>
</div>