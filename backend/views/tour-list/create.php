<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\TourList */

$this->title = 'Create Tour List';
$this->params['breadcrumbs'][] = ['label' => 'Tour Lists', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tour-list-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
