<?php
use yii\helpers\Html;

?>
<!-- EVENT TICKET - START -->


<div class="col-xs-12">

    <div class="event-ticket " style="border-bottom: 2px solid #d0eded;padding: 20px 0;">
        <div class="row">
            <div class="col-sm-12">
                <div class="event-ticket-massage">
                    <div class="massage-info">
                        <span><i class="fa fa-calendar"></i>Тривалість -  <?=$model->date_entrance.' - '.$model->date_departure ;?></span><br>
                                     <span><i class="fa fa-money"></i>Ціна - <?= $model->price;?></span><br>
                        <span><i class="fa fa-globe"></i>Країна - <?=$model->country;?></span><br>
                        <span><i class="fa fa-map-marker"></i>Місто - <?=$model->city;?></span>

                    </div>
                    <div class="massage-text">
                        <h3><?=$model->name;?></h3>

                            <img src="<?=$model->image;?>" style="max-width: 900px; width: 100%"><br>


                        <?=$model->description;?>


                        <p>
                            <?= Html::a('Редагувати', ['update', 'id' => $model->tour_list_id], ['class' => 'btn btn-primary']) ?>
                            <?= Html::a('Видалити', ['delete', 'id' => $model->tour_list_id], [
                                'class' => 'btn btn-danger',
                                'data' => [
                                    'confirm' => 'Are you sure you want to delete this item?',
                                    'method' => 'post',
                                ],
                            ]) ?>
                        </p>
                    </div>


                </div>
            </div>

        </div>
    </div>
</div>
<!-- EVENT TICKET - END -->