<?php
use yii\helpers\Html;

?>

<div class="col-xs-12" style="border-bottom: 2px solid #d0eded;padding: 20px 0;">
    <div class="event-ticket ">
        <div class="row">
            <div class="col-sm-12">
                <div class="event-ticket-massage">
                    <div class="massage-info" >
                        <p><?='<b>Замовник</b> - '.$model->user[0]->firstname.' '.$model->user[0]->secondname;?></p>
                        <p><?='<b>Номер телефону</b>'.$model->user[0]->phone;?></p>
                        <p><?='<b>Назва туру</b> - '.$model->tours[0]->name;?></p>
                        <p><?='<b>Кількість путівок -</b>'.$model->count ?></p>
                        <p><?='<b>Загальна вартість - </b>'.$model->tours[0]->price * $model->count ?></p>
                    </div>
                    <div class="massage-text">

                    </div>
                    <div class="massage-info">

                        <div id="qwerty<?=$model->order_id?>" style="display: none"><?= $model->status ?></div>
                        <?php

                        $this->registerJs('
                            var a = parseInt($("#qwerty'.$model->order_id.'").html());

                            if (a == 1){
                            $("#buttonAction'.$model->order_id.'").html("<a class=\'btn btn-primary\' href=\'/admin/order/public?id='.$model->order_id.'\' style=\'margin-top:20px\' data-confirm=\'Бажаєте оформити замовлення?\' data-method=\'post\'>Виконати</a><a class=\'btn btn-success\' href=\'/admin/order/delete?id='.$model->order_id.'\' style=\'margin-top:20px\' data-confirm=\'Ви дійсно бажаєте видалити замовлення?\' data-method=\'post\'>Видалити</a>");

                            }
                            else {
                            $("#buttonAction'.$model->order_id.'").html("<div class=\'panel panel-info\' style=\'max-width: 190px\'><div class=\'panel-heading\' >Замовлення виконано!</div></div>");
                            }
                        ');
                        ?>

                        <div id="buttonAction<?=$model->order_id?>"></div>





                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


