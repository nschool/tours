<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "tour_list".
 *
 * @property integer $tour_list_id
 * @property string $name
 * @property string $description
 * @property string $price
 * @property string $country
 * @property string $city
 * @property string $date_entrance
 * @property string $date_departure
 */
class TourList extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $file;
    public static function tableName()
    {
        return 'tour_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['price'], 'number'],
            [['date_entrance', 'date_departure','image'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['country', 'city'], 'string', 'max' => 30],
            [['file'], 'file'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'tour_list_id' => 'Tour List ID',
            'name' => 'Name',
            'description' => 'Description',
            'price' => 'Price',
            'country' => 'Country',
            'city' => 'City',
            'image' => 'Файл',
            'date_entrance' => 'Date Entrance',
            'date_departure' => 'Date Departure',
        ];
    }
}
