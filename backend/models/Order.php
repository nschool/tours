<?php

namespace backend\models;

use Yii;
use frontend\models\TourList;
use common\models\User;


/**
 * This is the model class for table "order".
 *
 * @property integer $order_id
 * @property integer $user_id
 * @property integer $tour_list_id
 * @property integer $count
 * @property integer $status
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'tour_list_id', 'count', 'status'], 'required'],
            [['user_id', 'tour_list_id', 'count', 'status'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'order_id' => 'Order ID',
            'user_id' => 'User ID',
            'tour_list_id' => 'Tour List ID',
            'count' => 'Count',
            'status' => 'Status',
        ];
    }

    public function getTours()
    {
        return $this->hasMany(TourList::className(), ['tour_list_id' => 'tour_list_id']);
    }
    public function getUser()
    {
        return $this->hasMany(User::className(), ['id' => 'user_id']);
    }
}
