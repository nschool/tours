<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\TourList;

/**
 * TourListSearch represents the model behind the search form about `backend\models\TourList`.
 */
class TourListSearch extends TourList
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tour_list_id'], 'integer'],
            [['name', 'description', 'country','image', 'city', 'date_entrance', 'date_departure'], 'safe'],
            [['price'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TourList::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'tour_list_id' => $this->tour_list_id,
            'price' => $this->price,
            'date_entrance' => $this->date_entrance,
            'date_departure' => $this->date_departure,
        ]);

        $query->andFilterWhere(['like', 'name', $this->name])
            ->andFilterWhere(['like', 'description', $this->description])
            ->andFilterWhere(['like', 'country', $this->country])
            ->andFilterWhere(['like', 'city', $this->city]);


        return $dataProvider;
    }
}
