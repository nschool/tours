<?php

namespace backend\controllers;

use Yii;
use backend\models\TourList;
use backend\models\Order;
use backend\models\TourListSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\UploadedFile;

/**
 * TourListController implements the CRUD actions for TourList model.
 */
class TourListController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    public function beforeAction($action)
    {
        $usernames = ['admin'];

        if ($action->id=='error') {
            $this->layout ='outsource';
            return true;
        }

        if (parent::beforeAction($action)) {
// if () {
// $this->layout = 'outsource';
// throw new ForbiddenHttpException('Доступ ограничен');
// }
//
            if (!Yii::$app->user->isGuest && !in_array(\Yii::$app->user->identity->username, $usernames)) {
                $this->layout = 'outsource';
                return $this->redirect('/site/index');
            }

        } else {
            return false;
        }

        return true;
    }

    /**
     * Lists all TourList models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new Order();
        $searchModel = new TourListSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'model' => $model
        ]);
    }

    /**
     * Displays a single TourList model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TourList model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TourList();

        if ($model->load(Yii::$app->request->post())) {



            if($model->file= UploadedFile::getInstance($model,'file'))
            {
                $path = Yii::getAlias('@frontend') .'/web/uploads';
                $imageName = Yii::$app->getSecurity()->generateRandomString();
                $model->file->saveAs( $path.'/'.$imageName.'.'.$model->file->extension);
                $model->image = '/uploads/'.$imageName.'.'.$model->file->extension;

            }
            $model->save();
            return $this->redirect(['view', 'id' => $model->tour_list_id]);

        }




        else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }

//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->tour_list_id]);
//        } else {
//            return $this->render('create', [
//                'model' => $model,
//            ]);
//        }
    }

    /**
     * Updates an existing TourList model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post())) {

            $oldimage= $model->image;

            if($model->file= UploadedFile::getInstance($model,'file'))
            {
                @unlink(Yii::getAlias('@frontend') . '/web' . $oldimage);
                $path = Yii::getAlias('@frontend') .'/web/uploads';
                $imageName = Yii::$app->getSecurity()->generateRandomString();
                $model->file->saveAs( $path.'/'.$imageName.'.'.$model->file->extension);
                $model->image = '/uploads/'.$imageName.'.'.$model->file->extension;

            }

            $model->save();
            return $this->redirect(['view', 'id' => $model->tour_list_id]);
        }
        else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
//        if ($model->load(Yii::$app->request->post()) && $model->save()) {
//            return $this->redirect(['view', 'id' => $model->tour_list_id]);
//        } else {
//            return $this->render('update', [
//                'model' => $model,
//            ]);
//        }
    }

    /**
     * Deletes an existing TourList model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the TourList model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TourList the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TourList::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
