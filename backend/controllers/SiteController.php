<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use yii\web\ForbiddenHttpException;


/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }


    public function beforeAction($action)
    {
        $usernames = ['admin'];

        if ($action->id=='error') {
            $this->layout ='outsource';
            return true;
        }

        if (parent::beforeAction($action)) {
// if () {
// $this->layout = 'outsource';
// throw new ForbiddenHttpException('Доступ ограничен');
// }
//
            if (!Yii::$app->user->isGuest && !in_array(\Yii::$app->user->identity->username, $usernames)) {
                $this->layout = 'outsource';
                throw new ForbiddenHttpException('Доступ ограничен');

            }

        } else {
            return false;
        }

        return true;
    }
    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    public function actionIndex()
    {
//        return $this->render('index');
        return  $this->redirect('/admin/tour-list/index');
    }

    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
//
//    public function beforeAction($action)
//    {
//        $usernames = ['admin'];
//
//        if ($action->id=='error') {
//            $this->layout ='outsource';
//            return true;
//        }
//
//        if (parent::beforeAction($action)) {
////            if () {
////                $this->layout = 'outsource';
////                throw new ForbiddenHttpException('Доступ ограничен');
////            }
////
//            if (!Yii::$app->user->isGuest && !in_array(\Yii::$app->user->identity->username, $usernames)) {
//                $this->layout = 'outsource';
//                return  $this->redirect('/site/index');
//            }
//
//        } else {
//            return false;
//        }
//
//        return true;
//    }
}
