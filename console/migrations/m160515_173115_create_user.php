<?php

use yii\db\Migration;

/**
 * Handles the creation for table `user`.
 */
class m160515_173115_create_user extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('user', 'admin', 'INT(11)');
        $res = $this->insert('user', [
            'username' => 'admin',
            'auth_key' => Yii::$app->security->generateRandomKey(),
            'password_hash' => Yii::$app->security->generatePasswordHash('admin'),
            'email' => 'natasha@cifr.us',
            'status' => 10,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            'admin' => 1,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user');
    }
}
