<?php

use yii\db\Migration;

class m160518_143357_add_column_user extends Migration
{
    public function up()
    {
        $this->addColumn('user', 'firstname', 'TEXT');
        $this->addColumn('user', 'secondname', 'TEXT');
        $this->addColumn('user', 'phone', 'TEXT');

    }

    public function down()
    {
        echo "m160518_143357_add_column_user cannot be reverted.\n";

        return false;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
