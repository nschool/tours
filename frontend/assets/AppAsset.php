<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'http://code.jquery.com/ui/1.11.1/themes/smoothness/jquery-ui.css',
        'css/animate.css',
        'css/yamm.css',
        'css/jquery.bootstrap-touchspin.css',
        'css/owl.carousel.css',
        'css/owl.theme.css',
        'css/owl.transitions.css',
        'css/magnific-popup.css',
        'css/creative-brands.css',
        'css/color-switcher.css',
        'css/color.css',
        'css/custom.css',
        'css/font-awesome.min.css',
        'css/site.css',
    ];
    public $js = [

        'https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js',
        'https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js',
//        'http://code.jquery.com/jquery-latest.min.js',
//        'http://code.jquery.com/ui/1.11.1/jquery-ui.js',
        'js/bootstrap.min.js',
        'js/bootstrap-hover-dropdown.min.js',
        'js/waypoints.min.js',
        'js/owl.carousel.js',
        'js/isotope.pkgd.min.js',
        'js/jquery.magnific-popup.min.js',
        'js/creative-brands.js',
        'js/color-switcher.js',
        'js/jquery.countTo.js',
        'js/jquery.countdown.js',
        'js/custom.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
