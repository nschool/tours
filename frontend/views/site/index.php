<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="jumbotron jumbotron6 jumbotron-full-height">
    <div id="jumbotron-slider" class="owl-carousel hidden-control owl-theme owl-animation">

        <div class="item" id="slide-2">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div><h2 class="animation top-to-bottom delay-1">Турфірма</h2></div>
<!--                        <div><h3 class="animation bottom-to-top delay-2">пропонує</h3></div>-->
<!--                        <div><h3 class="animation bottom-to-top delay-3">понад 50 турів</h3></div>-->
                        <a href="<?=Url::toRoute("/tour-list/index")?>" class="btn btn-info btn-lg">Переглянути усі</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="item" id="slide-1">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h2 class="animation right-to-left fast delay-1">Кіпр</h2><br>
                        <h3 class="animation right-to-left fast delay-2">Ларнака</h3><br>
                        <h3 class="animation right-to-left fast delay-3">Sentido Sandy Beach Hotel</h3><br>
                        <h3 class="animation right-to-left fast delay-4">100 EUR</h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="item" id="slide-7">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-6">
                        <h2 class="animation right-to-left fast delay-1">ОАЕ</h2><br>
                        <h3 class="animation right-to-left fast delay-2">Дубаї</h3><br>
                        <h3 class="animation right-to-left fast delay-3">Hilton Fujairah Resort 5*</h3><br>
                        <h3 class="animation right-to-left fast delay-4">400 EUR</h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="item" id="slide-4">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <h2 class="animation right-to-left fast delay-1">Домінікана</h2><br>
                        <h3 class="animation right-to-left fast delay-2">Пунта Кана</h3><br>
                        <h3 class="animation right-to-left fast delay-3">Barcelo Bavaro Palace Deluxe 5*</h3><br>
                        <h3 class="animation right-to-left fast delay-4">400 EUR</h3>
                    </div>
                </div>
            </div>
        </div>
        <div class="item" id="slide-5">
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-6">
                        <h2 class="animation right-to-left fast delay-1">Туреччина</h2><br>
                        <h3 class="animation right-to-left fast delay-2">Анталія</h3><br>
                        <h3 class="animation right-to-left fast delay-3">Atan Park Hotel</h3><br>
                        <h3 class="animation right-to-left fast delay-4">200 EUR</h3>
                    </div>
                </div>
            </div>
        </div>

    </div>
</div>

<div class="container">
    <div style="padding: 50px 0">
    <?php
    if (Yii::$app->user->isGuest) {
        echo 'Щоб зробити замовлення слід <a href="'.Url::toRoute("/site/signup").'">зареєстуватися</a>
        або <a href="'.Url::toRoute("/site/login").'">увійти</a> на сайт';
    }else{
    ?>
    <?= $this->render('/order/_form', [
        'model' => $model
    ]) ?>
    <?php
    }
    ?>
    </div>
</div>

