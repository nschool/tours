<?php
use yii\helpers\Html;

?>
<!-- EVENT TICKET - START -->


<div class="col-xs-12">

    <div class="event-ticket ">
        <div class="row">
            <div class="col-sm-12">
                <div class="event-ticket-massage">
                    <div class="massage-info">
                        <span><i class="fa fa-calendar"></i>Тривалість -  <?=$model->tours[0]->date_entrance.' - '.$model->tours[0]->date_departure ;?></span>
                        <span><i class="fa fa-user"></i>Кількість путівок - <?=$model->count;?></span>
                        <span><i class="fa fa-money"></i>Ціна - <?= $model->tours[0]->price;?></span>
                        <span><i class="fa fa-money"></i>Загальна вартість - <?=$model->count * $model->tours[0]->price;?></span>

                    </div>
                    <div class="massage-text">
                        <h3><?=$model->tours[0]->name;?></h3>
                        <?=$model->tours[0]->description;?>
                    </div>
                    <div class="massage-info">
                        <div id="qwerty<?=$model->order_id?>" style="display: none"><?= $model->status ?></div>
                        <?php

                        $this->registerJs('
                            var a = parseInt($("#qwerty'.$model->order_id.'").html());

                            if (a == 1){
                            $("#buttonAction'.$model->order_id.'").html("<a class=\'btn btn-primary\' href=\'/admin/order/public?id='.$model->order_id.'\' style=\'margin-top:20px\' data-confirm=\'Бажаєте оформити замовлення?\' data-method=\'post\'>Виконати</a><a class=\'btn btn-success\' href=\'/admin/order/delete?id='.$model->order_id.'\' style=\'margin-top:20px\' data-confirm=\'Ви дійсно бажаєте видалити замовлення?\' data-method=\'post\'>Видалити</a>");

                            }
                            else {
                            $("#buttonAction'.$model->order_id.'").html("<div class=\'panel panel-info\' style=\'max-width: 190px\'><div class=\'panel-heading\' >Замовлення виконано!</div></div>");
                            }
                        ');
                        ?>

                        <div id="buttonAction<?=$model->order_id?>"></div>

                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
<!-- EVENT TICKET - END -->