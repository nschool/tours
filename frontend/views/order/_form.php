<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use frontend\models\TourList;

/* @var $this yii\web\View */
/* @var $model backend\models\Order */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="order-form">

    <?php $form = ActiveForm::begin([
        'method' => 'post',
        'action' => Url::toRoute('/order/create'),
        'options' => ['enctype' => 'multipart/form-data',
            'id' => 'form1']

    ]); ?>

    <?= $form->field($model, 'user_id')->hiddenInput(['value'=>Yii::$app->user->identity->id ])->label(false) ?>

<div class="row">
    <div class="col-md-4">
      <?= $form->field($model, 'tour_list_id')->dropDownList(
                                                        ArrayHelper::map(TourList::find()->all(),
                                                            'tour_list_id', 'name'),
                                                        ['prompt' => 'Обрати тур'],
                                                        ['class'=>'select_departament']);
        //                                            ?>

    </div>
    <div class="col-md-4">
        <?= $form->field($model, 'count')->textInput() ?>

    </div>
    <div class="col-md-4">
        <?= $form->field($model, 'status')->hiddenInput(['value'=>1])->label(false) ?>



        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? 'Замовити' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>


    </div>
</div>


    <?php ActiveForm::end(); ?>

</div>
