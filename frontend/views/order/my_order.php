<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel backend\models\OrderSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Мої замовленні тури';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
    <div class="order-index">

        <h1><?= Html::encode($this->title) ?></h1>


        <?php
//        print_r($dataProvider);die();
        ?>
        <?  Pjax::begin();?>
        <?= ListView::widget([
            'dataProvider' => $dataProvider,
            'itemOptions' => ['class' => 'item'],
            'itemView' => function ($model, $key, $index) {

                return  $this->render('_view', ['model' => $model, 'index'=>$index]);

            },
            'pager' => [
                'firstPageLabel' => '<i class="fa fa-angle-double-left"></i>',
                'lastPageLabel' => '<i class="fa fa-angle-double-right"></i>',
                'prevPageLabel' => '<i class="fa  fa-angle-left"></i>',
                'nextPageLabel' => '<i class="fa  fa-angle-right"></i>',
                'maxButtonCount' => 3,
                'options' => [
                    'tag' => 'ul',
                    'class' => 'pagination',
                ],
            ],
        ]) ?>

        <?  Pjax::end();?>

    </div>
</div>