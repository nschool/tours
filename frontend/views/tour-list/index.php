<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\widgets\Pjax;
use yii\widgets\ListView;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\TourListSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tour Lists';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container">
<div class="tour-list-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>



    <div style="padding: 50px 0">
        <?php
        if (Yii::$app->user->isGuest) {
            echo 'Щоб зробити замовлення слід <a href="'.Url::toRoute("/site/signup").'">зареєстуватися</a>
        або <a href="'.Url::toRoute("/site/login").'">увійти</a> на сайт';
        }else{
            ?>
            <?= $this->render('/order/_form', [
                'model' => $model
            ]) ?>
            <?php
        }
        ?>
    </div>
    <?  Pjax::begin();?>
    <?= ListView::widget([
        'dataProvider' => $dataProvider,
        'itemOptions' => ['class' => 'item'],
        'itemView' => function ($model, $key, $index) {

            return  $this->render('_view', ['model' => $model, 'index'=>$index]);

        },
        'pager' => [
            'firstPageLabel' => '<i class="fa fa-angle-double-left"></i>',
            'lastPageLabel' => '<i class="fa fa-angle-double-right"></i>',
            'prevPageLabel' => '<i class="fa  fa-angle-left"></i>',
            'nextPageLabel' => '<i class="fa  fa-angle-right"></i>',
            'maxButtonCount' => 3,
            'options' => [
                'tag' => 'ul',
                'class' => 'pagination',
            ],
        ],
    ]) ?>

    <?  Pjax::end();?>



</div>
</div>