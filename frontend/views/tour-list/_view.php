<?php
use yii\helpers\Html;

?>
<!-- EVENT TICKET - START -->


<div class="col-xs-4">

    <div class="event-ticket" style="height:400px">
        <div class="row">
            <div class="col-sm-12">
                <div class="event-ticket-massage">
                    <div class="massage-info" ">
                        <span><i class="fa fa-calendar" ></i> <span style="font-size:10px">Тривалість -  <?=$model->date_entrance.' - '.$model->date_departure ;?></span></span><br>
                                     <span><i class="fa fa-money"></i> <span style="font-size:10px">Ціна - <?= $model->price;?> EUR</span></span><br>
                        <span><i class="fa fa-globe"></i> <span style="font-size:10px">Країна - <?=$model->country;?></span></span><br>
                        <span><i class="fa fa-map-marker"></i> <span style="font-size:10px">Місто - <?=$model->city;?></span></span><br>

                    </div>
                    <div class="massage-text">

                            <div style="margin: 0 auto">
                            <img src="<?=$model->image;?>" style="height: 180px; max-width: 100%">
                            </div>


                        <h3 style="font-size:18px"><?=$model->name;?></h3>
                        <span style="font-size:13px"><?=$model->description;?></span>
                    </div>


                </div>
            </div>

        </div>
    </div>
</div>
<!-- EVENT TICKET - END -->