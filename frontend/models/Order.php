<?php

namespace frontend\models;

use frontend\models\TourList;
use Yii;
//use frontend\models\TourList;

/**
 * This is the model class for table "order".
 *
 * @property integer $order_id
 * @property integer $user_id
 * @property integer $tour_list_id
 * @property integer $count
 * @property integer $status
 */
class Order extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'tour_list_id', 'count', 'status'], 'required'],
            [['user_id', 'tour_list_id', 'count', 'status'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'order_id' => 'Order ID',
            'user_id' => 'User ID',
            'tour_list_id' => 'Тур',
            'count' => 'Кількість путівок',
            'status' => 'Status',
        ];
    }
    public function getTours()
    {
        return $this->hasMany(TourList::className(), ['tour_list_id' => 'tour_list_id']);
    }
}
